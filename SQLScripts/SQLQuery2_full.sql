USE [C:\SQLDATABASE\RSSDatabase.mdf]
GO
/****** Object:  Table [dbo].[NewsTable]    Script Date: 05/06/2014 15:38:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTable](
	[FeedName] [nchar](20) NOT NULL,
	[News] [nchar](100) NOT NULL,
	[URLnews] [nchar](200) NOT NULL,
 CONSTRAINT [PK_NewsTable] PRIMARY KEY CLUSTERED 
(
	[News] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[NewsTable] ([FeedName], [News], [URLnews]) VALUES (N'NEW                 ', N'30 Vital Steps To Follow After Installing WordPress                                                 ', N'http://feeds.dzone.com/~r/dzone/frontpage/~3/vutePPOcT10/30_vital_steps_to_follow_after_installing_wordpre.html                                                                                         ')
INSERT [dbo].[NewsTable] ([FeedName], [News], [URLnews]) VALUES (N'NYT                 ', N'A Deadly Fungus and Questions at a Hospital                                                         ', N'http://www.nytimes.com/2014/04/29/us/a-deadly-fungus-and-questions-at-a-hospital.html?partner=rss&emc=rss                                                                                               ')
INSERT [dbo].[NewsTable] ([FeedName], [News], [URLnews]) VALUES (N'NYT                 ', N'A Grim Toll as Storms Sweep South and Midwest                                                       ', N'http://www.nytimes.com/2014/04/30/us/deadly-storms-sweep-south-and-midwest.html?partner=rss&emc=rss                                                                                                     ')
INSERT [dbo].[NewsTable] ([FeedName], [News], [URLnews]) VALUES (N'NYT                 ', N'Administration Begins Search for New Contractors to Run Health Care Site                            ', N'http://www.nytimes.com/2014/04/29/us/administration-begins-search-for-new-contractors-to-run-health-care-site.html?partner=rss&emc=rss                                                                  ')

/****** Object:  Table [dbo].[FeedsTable]    Script Date: 05/06/2014 15:38:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeedsTable](
	[FeedName] [nvarchar](20) NOT NULL,
	[URL] [nchar](200) NOT NULL,
 CONSTRAINT [PK_FeedsTable] PRIMARY KEY CLUSTERED 
(
	[FeedName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[FeedsTable] ([FeedName], [URL]) VALUES (N'HABR', N'http://habrahabr.ru                                                                                                                                                                                     ')
INSERT [dbo].[FeedsTable] ([FeedName], [URL]) VALUES (N'NEW', N'http://feeds.dzone.com/dzone/frontpage                                                                                                                                                                  ')
INSERT [dbo].[FeedsTable] ([FeedName], [URL]) VALUES (N'NYT', N'http://feeds.nytimes.com/nyt/rss/homepage                                                                                                                                                               ')
