﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using RssProject.ServiceRss;

namespace RssProject
{
    public partial class FetchNewsWindow : ChildWindow
    {
        //public ObservableCollection<RssSource> mySource;
        public ObservableCollection<RssNews> NewsSource;

        public FetchNewsWindow()
        {
            InitializeComponent();

            // Start getting news from SQL database
            ServiceRss.ServiceRSSClient RssClient = new ServiceRss.ServiceRSSClient();
            RssClient.GetNewsCompleted += new EventHandler<ServiceRss.GetNewsCompletedEventArgs>(GetNewsCompleted);
            RssClient.GetNewsAsync();
        }

        // update Feeds Table in main window
        private void GetNewsCompleted(object sender, ServiceRss.GetNewsCompletedEventArgs e)
        {
            try
            {
                NewsSource = e.Result;
                NewsDataGrid.ItemsSource = NewsSource;
                MessageBox.Show("Success!");
            }
            catch
            {
                MessageBox.Show("Error getting data from database.");
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////
        // delete all news from NewsTable of database
        private void DeleteAllButton_Click(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = false;
            ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
            client.DelAllNewsCompleted += new EventHandler<ServiceRss.DelAllNewsCompletedEventArgs>(DelAllNewsCompleted);
            client.DelAllNewsAsync();
        }
        //
        private void DelAllNewsCompleted(object sender, ServiceRss.DelAllNewsCompletedEventArgs e)
        {
            try
            {
                MessageBox.Show(e.Result.ToString());
                // get new data from NewsTable of database
                ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
                client.GetNewsCompleted += new EventHandler<ServiceRss.GetNewsCompletedEventArgs>(GetNewsCompleted);
                client.GetNewsAsync();       
            }
            catch
            {
                MessageBox.Show("Error when delete all data. Fault answer.");
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////
        // Group for update news list
        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = false;
            ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
            client.FetchAllFeedNewsCompleted += new EventHandler<ServiceRss.FetchAllFeedNewsCompletedEventArgs>(FetchAllFeedNewsCompleted);
            client.FetchAllFeedNewsAsync();
        }
        
        private void FetchAllFeedNewsCompleted(object sender, ServiceRss.FetchAllFeedNewsCompletedEventArgs e)
        {
            try
            {
                if (e.Result != "true")
                {
                    MessageBox.Show(e.Result);
                }
                // get new data from NewsTable of database
                ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
                client.GetNewsCompleted += new EventHandler<ServiceRss.GetNewsCompletedEventArgs>(GetNewsCompleted);
                client.GetNewsAsync(); 
            }
            catch
            {
                MessageBox.Show("Error when was executed update news. Fault service answer.");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        // Group for delete 1 news
        private void DeleteNewsButton_Click(object sender, RoutedEventArgs e)
        {
            // define Row index and get news Name
            Button btn = sender as Button;
            var dataRow = (RssNews)btn.DataContext;
            // delete Row from database
            ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
            client.DelNewsCompleted += new EventHandler<ServiceRss.DelNewsCompletedEventArgs>(DelNewsCompleted);
            client.DelNewsAsync(dataRow.NewsName);
        }

        private void DelNewsCompleted(object sender, ServiceRss.DelNewsCompletedEventArgs e)
        {
            try
            {
                MessageBox.Show(e.Result.ToString());
                // get new data from database
                ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
                client.GetNewsCompleted += new EventHandler<ServiceRss.GetNewsCompletedEventArgs>(GetNewsCompleted);
                client.GetNewsAsync();       
            }
            catch
            {
                MessageBox.Show("Error when delete this Row in database. Fault service answer.");
            }
        }
    }

}

