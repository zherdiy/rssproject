﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Markup;
using System.Collections.ObjectModel;
using RssProject.ServiceRss;

namespace RssProject
{
    public partial class MainPage : UserControl
    {
        public ObservableCollection<Feed> feeds;

        public MainPage()
        {
            InitializeComponent();

            // for install process
            Loaded += new RoutedEventHandler(MainPage_Loaded);
            Application.Current.InstallStateChanged += new EventHandler(OnInstallStateChanged);

            feeds = new ObservableCollection<Feed>();        
 
            // Start getting feeds from SQL database
            ServiceRss.ServiceRSSClient RssClient = new ServiceRss.ServiceRSSClient();
            RssClient.GetFeedsCompleted += new EventHandler<ServiceRss.GetFeedsCompletedEventArgs>(GetFeedsCompleted);
            RssClient.GetFeedsAsync();
        }

        void OnInstallStateChanged(object sender, EventArgs e) 
        {
            switch (Application.Current.InstallState) 
            {
                case InstallState.InstallFailed: break; 
                case InstallState.Installed: ToggleInstallLinks(true); break; 
                case InstallState.Installing: break; 
                case InstallState.NotInstalled: ToggleInstallLinks(false); break; 
                default: break; 
            } 
        }
        // 
        private void ToggleInstallLinks(bool hidden) 
        { 
            InstallLink.Visibility = hidden ? Visibility.Collapsed : Visibility.Visible; 
        }
        // Check: if working in browser?
        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.Current.IsRunningOutOfBrowser)
            {
                ToggleInstallLinks(true);
            }
            else
            {
                if (App.Current.InstallState == InstallState.Installed)
                {
                    ToggleInstallLinks(true);
                }
            }
        }

        // update Feeds Table in main window
        private void GetFeedsCompleted(object sender, ServiceRss.GetFeedsCompletedEventArgs e)
        {
            try
            {
                feeds = e.Result;
                FeedsDataGrid.ItemsSource = feeds;
                MessageBox.Show("Success!");
            }
            catch
            {
                MessageBox.Show("Error getting data from database.");
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////
        // Group for add feed
        // Open child window AddFeedWindow
        private void AddFeedButton_Click(object sender, RoutedEventArgs e)
        {
            AddFeedWindow afw = new AddFeedWindow();    // child window instance
            afw.Closed += new EventHandler(afw_Closed); // handler for close child window
            afw.Show();                                 // start show child window
        }
        
        // After close AddFeed Window  will perform this handler
        void afw_Closed(object sender, EventArgs e)
        {
            AddFeedWindow child = (AddFeedWindow)sender;

            if (child.DialogResult == true)
            {
                if (child.AddFeedName.Text != string.Empty && child.AddFeedUrl.Text != string.Empty)
                {
                    //this.feeds.Add(new Feed(child.AddFeedName.Text, new Uri(child.AddFeedUrl.Text)));
                    // refresh database
                    ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
                    client.AddFeedCompleted += new EventHandler<ServiceRss.AddFeedCompletedEventArgs>(AddFeedCompleted);
                    client.AddFeedAsync(child.AddFeedName.Text, child.AddFeedUrl.Text);
                    
                }
                else
                {
                    MessageBox.Show("You didn't fill Name or URL field");
                }
            }
        }
        
        // start getting new data from database after add new feed
        private void AddFeedCompleted(object sender, ServiceRss.AddFeedCompletedEventArgs e)
        {
            try
            {
                MessageBox.Show(e.Result.ToString());
                // get new data from database
                ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
                client.GetFeedsCompleted += new EventHandler<ServiceRss.GetFeedsCompletedEventArgs>(GetFeedsCompleted);
                client.GetFeedsAsync();
            }
            catch
            {
                MessageBox.Show("Error when add new data to database. Fault answer.");
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////
        // Group for fetch news
        // Open child window FetchNewsWindow
        private void FetchNewsButton_Click(object sender, RoutedEventArgs e)
        {
            FetchNewsWindow fnw = new FetchNewsWindow();    // child window instance
            fnw.Show();                                 // start show child window
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        // Group for delete 1 feed
        private void DeleteFeedButton_Click(object sender, RoutedEventArgs e)
        {
            // define Row index and get feed Name
            Button btn = sender as Button;
            var dataRow = (Feed)btn.DataContext;

            // delete Row from database
            ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
            client.DelFeedCompleted += new EventHandler<ServiceRss.DelFeedCompletedEventArgs>(DelFeedCompleted);
            client.DelFeedAsync(dataRow.Name.ToString());
        }

        private void DelFeedCompleted(object sender, ServiceRss.DelFeedCompletedEventArgs e)
        {
            try
            {
                MessageBox.Show(e.Result.ToString());
                // get new data from database
                ServiceRss.ServiceRSSClient client = new ServiceRss.ServiceRSSClient();
                client.GetFeedsCompleted += new EventHandler<ServiceRss.GetFeedsCompletedEventArgs>(GetFeedsCompleted);
                client.GetFeedsAsync();
            }
            catch
            {
                MessageBox.Show("Error when delete Row in database. Fault answer from service.");
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        // Handler for Instal Link Button
        private void InstallOffline(Object sender, RoutedEventArgs e)
        {
            if (Application.Current.Install()) // start to install application
            {
                // Hide Link for instal programm
                InstallLink.Visibility = Visibility.Collapsed;
            }
        }

    }
}
