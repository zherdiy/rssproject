﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;  // AspNetCompatibilityRequirements
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Collections.ObjectModel;  // ObservableCollection
using System.ServiceModel.Syndication;
using System.Net;
using System.Xml;
using System.IO; // RSS Dll

namespace RssProject.Web
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ServiceRSS
    {
        // Contract for get all data from Feed Table of SQL DB 
        [OperationContract]
        public ObservableCollection<Feed> GetFeeds()
        {
            ObservableCollection<Feed> feeds = new ObservableCollection<Feed>();
            // Create connection string 
            
            SqlConnection thisConnection = new SqlConnection(
                @"Data Source=.\SQLEXPRESS;" +
                @"AttachDbFilename=C:\SQLDataBase\ANDatabase.mdf;" +
                @"Integrated Security=True; Connect Timeout=30; User Instance=true");
            thisConnection.Open();
            // Create object DataAdapter for renew and others operations 
            SqlDataAdapter thisAdapter = new SqlDataAdapter("SELECT FeedName, URL FROM FeedsTable", thisConnection);
            // Создать объект CommandBuilder для построения команд SQL 
            SqlCommandBuilder thisBuilder = new SqlCommandBuilder(thisAdapter);
            // Создать DataSet для хранения таблиц данных, строк и столбцов 
            DataSet thisDataSet = new DataSet();
            // Заполнить DataSet, используя запрос, определенный ранее в DataAdapter 
            thisAdapter.Fill(thisDataSet, "FeedsTable");
            //
            foreach (DataRow Row in thisDataSet.Tables["FeedsTable"].Rows)
            {
                feeds.Add(new Feed(Row["FeedName"].ToString(), new Uri(Row["URL"].ToString())));
            }
            //
            thisConnection.Close();
            return feeds;
        }

        // Contract for get all news from NewsTable of SQL database
        [OperationContract]
        public ObservableCollection<RssNews> GetNews()
        {
            ObservableCollection<RssNews> news = new ObservableCollection<RssNews>();
            // Create connection string 
            SqlConnection thisConnection = new SqlConnection(
                @"Data Source=.\SQLEXPRESS;" +
                @"AttachDbFilename='C:\SQLDataBase\ANDatabase.mdf';" +
                @"Integrated Security=True; Connect Timeout=30; User Instance=true");
            thisConnection.Open();
            // Create object DataAdapter for renew and others operations 
            SqlDataAdapter thisAdapter = new SqlDataAdapter("SELECT FeedName, News, URLnews FROM NewsTable", thisConnection);
            // Создать объект CommandBuilder для построения команд SQL 
            SqlCommandBuilder thisBuilder = new SqlCommandBuilder(thisAdapter);
            // Создать DataSet для хранения таблиц данных, строк и столбцов 
            DataSet thisDataSet = new DataSet();
            // Заполнить DataSet, используя запрос, определенный ранее в DataAdapter 
            thisAdapter.Fill(thisDataSet, "NewsTable");
            //
            foreach (DataRow Row in thisDataSet.Tables["NewsTable"].Rows)
            {
                news.Add(new RssNews(Row["FeedName"].ToString(), Row["News"].ToString(), new Uri(Row["URLnews"].ToString())));
            }
            //
            thisConnection.Close();
            return news;
        }

        // Contract for add 1 row to Feed Table of SQL DB 
        [OperationContract]
        public string AddFeed(string Name, string Url)
        {
            string result;
            // Create connection string 
            SqlConnection thisConnection = new SqlConnection(
                @"Data Source=.\SQLEXPRESS;" +
                @"AttachDbFilename='C:\SQLDataBase\ANDatabase.mdf';" +
                @"Integrated Security=True; Connect Timeout=30; User Instance=true");
            thisConnection.Open();
            // Create object DataAdapter for renew and others operaions 
            SqlDataAdapter thisAdapter = new SqlDataAdapter("SELECT FeedName, URL FROM FeedsTable", thisConnection);
            // Создать объект CommandBuilder для построения команд SQL 
            SqlCommandBuilder thisBuilder = new SqlCommandBuilder(thisAdapter);
            // Создать DataSet для хранения таблиц данных, строк и столбцов 
            DataSet thisDataSet = new DataSet();
            // Заполнить DataSet, используя запрос, определенный ранее в DataAdapter 
            thisAdapter.Fill(thisDataSet, "FeedsTable");
            // Set primary key  for find command
            thisDataSet.Tables["FeedsTable"].PrimaryKey = new DataColumn[] { thisDataSet.Tables["FeedsTable"].Columns["FeedName"] };
            // find string
            DataRow findRow = thisDataSet.Tables["FeedsTable"].Rows.Find(Name);
            //
            if (findRow != null)
            {
                // searching row already exist. we don't neet add new row to database
                result = "This URL already exist.";
            }
            else
            {
                // add new row to database
                DataRow thisRow = thisDataSet.Tables["FeedsTable"].NewRow(); // new empty row
                thisRow["FeedName"] = Name;
                thisRow["URL"] = Url;
                thisDataSet.Tables["FeedsTable"].Rows.Add(thisRow); // renew DataSet
                thisAdapter.Update(thisDataSet, "FeedsTable");      // update table in DB
                result = "Success!";
            }
            thisConnection.Close();
            return result;
        }

        // Contract for delete 1 row of Feed Table in SQL DB 
        [OperationContract]
        public string DelFeed(string Name)
        {
            string result;
            // Create connection string 
            SqlConnection thisConnection = new SqlConnection(
                @"Data Source=.\SQLEXPRESS;" +
                @"AttachDbFilename='C:\SQLDataBase\ANDatabase.mdf';" +
                @"Integrated Security=True; Connect Timeout=30; User Instance=true");
            thisConnection.Open();
            // Create object DataAdapter for renew and others operations 
            SqlDataAdapter thisAdapter = new SqlDataAdapter("SELECT FeedName, URL FROM FeedsTable", thisConnection);
            // Создать объект CommandBuilder для построения команд SQL 
            SqlCommandBuilder thisBuilder = new SqlCommandBuilder(thisAdapter);
            // Создать DataSet для хранения таблиц данных, строк и столбцов 
            DataSet thisDataSet = new DataSet();
            // Заполнить DataSet, используя запрос, определенный ранее в DataAdapter 
            thisAdapter.Fill(thisDataSet, "FeedsTable");
            // Set primary key  for find command
            thisDataSet.Tables["FeedsTable"].PrimaryKey = new DataColumn[] { thisDataSet.Tables["FeedsTable"].Columns["FeedName"] };
            // find string
            DataRow findRow = thisDataSet.Tables["FeedsTable"].Rows.Find(Name);
            // delete Row
            if (findRow != null)
            {
                findRow.Delete();
                thisAdapter.Update(thisDataSet, "FeedsTable");
                result = "Success!";
            }
            else 
                result = "Error deleting feed.";

            thisConnection.Close();
            return result;
        }

        // Contract for delete 1 row of News Table in SQL DB 
        [OperationContract]
        public string DelNews(string newsname)
        {
            string result;
            // Create connection string 
            SqlConnection thisConnection = new SqlConnection(
                @"Data Source=.\SQLEXPRESS;" +
                @"AttachDbFilename='C:\SQLDataBase\ANDatabase.mdf';" +
                @"Integrated Security=True; Connect Timeout=30; User Instance=true");
            thisConnection.Open();
            // Create object DataAdapter for update database and others operations 
            SqlDataAdapter thisAdapter = new SqlDataAdapter("SELECT FeedName, News, URLnews FROM NewsTable", thisConnection);
            // Создать объект CommandBuilder для построения команд SQL 
            SqlCommandBuilder thisBuilder = new SqlCommandBuilder(thisAdapter);
            // Создать DataSet для хранения таблиц данных, строк и столбцов 
            DataSet thisDataSet = new DataSet();
            // Заполнить DataSet, используя запрос, определенный ранее в DataAdapter 
            thisAdapter.Fill(thisDataSet, "NewsTable");
            // Set primary key for find command
            thisDataSet.Tables["NewsTable"].PrimaryKey = new DataColumn[] { thisDataSet.Tables["NewsTable"].Columns["News"] };
            // find string
            DataRow findRow = thisDataSet.Tables["NewsTable"].Rows.Find(newsname);
            // delete Row
            if (findRow != null)
            {
                findRow.Delete();
                thisAdapter.Update(thisDataSet, "NewsTable");
                result = "Success!";
            }
            else
                result = "Error deleting news.";

            thisConnection.Close();

            return result;
        }

        // Contract for delete all rows of News Table in SQL database 
        [OperationContract]
        public string DelAllNews()
        {
            string result;
            // Create connection string 
            SqlConnection thisConnection = new SqlConnection(
                @"Data Source=.\SQLEXPRESS;" +
                @"AttachDbFilename='C:\SQLDataBase\ANDatabase.mdf';" +
                @"Integrated Security=True; Connect Timeout=30; User Instance=true");
            thisConnection.Open();
            // Create object DataAdapter for renew and others operations 
            SqlDataAdapter thisAdapter = new SqlDataAdapter("SELECT FeedName, News, URLnews FROM NewsTable", thisConnection);
            // Создать объект CommandBuilder для построения команд SQL 
            SqlCommandBuilder thisBuilder = new SqlCommandBuilder(thisAdapter);
            // Создать DataSet для хранения таблиц данных, строк и столбцов 
            DataSet thisDataSet = new DataSet();
            // Заполнить DataSet, используя запрос, определенный ранее в DataAdapter 
            thisAdapter.Fill(thisDataSet, "NewsTable");
            // Mark all rows to delete from News Table of database
            foreach (DataRow row in thisDataSet.Tables["NewsTable"].Rows)
            {
                row.Delete();
            }
            // Update database
            thisAdapter.Update(thisDataSet, "NewsTable");
            // Close connection
            thisConnection.Close();
            result = "Success!";
            
            return result;
        }

        // Contract for update news in News Table (NewsDataGrid)
        [OperationContract]
        public string FetchAllFeedNews()
        {
            string result;
            XmlReader xmlReader;
            SyndicationFeed rssfeed;

            /// fill dataset by data from database
            // Create connection string
            SqlConnection thisConnection = new SqlConnection(
                @"Data Source=.\SQLEXPRESS;" +
                @"AttachDbFilename='C:\SQLDataBase\ANDatabase.mdf';" +
                @"Integrated Security=True; Connect Timeout=30; User Instance=true");
            try
            {
                thisConnection.Open();
                result = "Success!";
            }
            catch
            {
                result = "Can't open SQL database";
            }
            // Create object DataAdapter for query and update 
            SqlDataAdapter feedAdapter = new SqlDataAdapter("SELECT FeedName, URL FROM FeedsTable", thisConnection);
            // Создать объект CommandBuilder для построения команд SQL 
            SqlCommandBuilder feedBuilder = new SqlCommandBuilder(feedAdapter);
            // Создать DataSet для хранения таблиц данных, строк и столбцов 
            DataSet thisDataSet = new DataSet();
            // add FeedsTable to DataSet 
            feedAdapter.Fill(thisDataSet, "FeedsTable");
            // Create object DataAdapter for query from news Table
            SqlDataAdapter newsAdapter = new SqlDataAdapter("SELECT FeedName, News, URLnews FROM NewsTable", thisConnection);
            // create CommandBuilder  
            SqlCommandBuilder newsBuilder = new SqlCommandBuilder(newsAdapter);
            // add NewsTable to DataSet
            newsAdapter.Fill(thisDataSet, "NewsTable");
            // Set primary key to News column for find command
            thisDataSet.Tables["NewsTable"].PrimaryKey = new DataColumn[] { thisDataSet.Tables["NewsTable"].Columns["News"] };
 
            /// get RSS data
            //           
            foreach (DataRow feed_row in thisDataSet.Tables["FeedsTable"].Rows)
            {
                rssfeed = null;
                try
                {   // retrieve XML string data from current site                
                    xmlReader = XmlReader.Create(feed_row["URL"].ToString());
                    // treats XML data like RSS
                    rssfeed = SyndicationFeed.Load(xmlReader);
                    xmlReader.Close();
                }
                catch
                {
                    result = "Error when open one or more RSS channels";
                }

                // disassemble data of current RSS
                if (rssfeed != null)
                {
                    foreach (SyndicationItem item in rssfeed.Items)
                    {
                        // find existing string
                        DataRow findRow = thisDataSet.Tables["NewsTable"].Rows.Find(item.Title.Text);
                        if (findRow == null)
                        {   // current news not exist
                            DataRow newRow = thisDataSet.Tables["NewsTable"].NewRow(); // new empty row
                            newRow["FeedName"] = feed_row["FeedName"];
                            newRow["News"] = item.Title.Text;
                            newRow["URLnews"] = item.Links[0].Uri.AbsoluteUri.ToString(); //item.BaseUri.AbsoluteUri.ToString(); 
                            thisDataSet.Tables["NewsTable"].Rows.Add(newRow); // update DataSet
                        }
                    }
                }  
            }
            // update NewsTable in database
            newsAdapter.Update(thisDataSet, "NewsTable");   
            
            // close connection           
            thisConnection.Close();
            
            //result = "true";
            return result;
        }
    }

    [DataContract]
    //[SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Feed
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Uri Link { get; set; }

        public Feed(string _Name, Uri _Link)
        {
            this.Name = _Name;
            this.Link = _Link;
        }
    }

    [DataContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RssNews
    {
        [DataMember]
        public string FeedName { get; set; }
        [DataMember]
        public string NewsName { get; set; }
        [DataMember]
        public Uri NewsLink { get; set; }

        public RssNews(string _feed, string _name, Uri _link)
        {
            this.FeedName = _feed;
            this.NewsName = _name;
            this.NewsLink = _link;
        }
    }

}
